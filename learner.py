LOCAL = True

def answers_to_tsv():
	# Generate the tsv string from list of answers and return it
	return "\n".join(["\t".join(answer) for answer in data_to_list()])

def data_to_list():
	# Generate list of answers from all the data given so far
	if LOCAL:
		f = open("training_data.tsv")
		data = [line.replace("\n","").split("\t") for line in f.readlines()]
		f.close()
		return data
	else:
		from models import Answer
		return [[			
                str(answer.meal.id),
                answer.meal.name,
                str(answer.answer),
                answer.queryid,
                answer.ip,
                str(answer.timestamp)
				] for answer in Answer.objects.all()]

def data_visualizer():
	import matplotlib.pyplot as plt

	data = data_to_list()
	data = [answer[:-1] + [datetime.strptime(answer[-1], "%Y-%m-%d %H:%M:%S.%f")] for answer in data]
	data.sort(key=lambda x: x[-1])

	# Generates a graph that shows the cumulative data
	data_as_seconds = [[(answer[-1]-data[0][-1]).total_seconds(), ind] for ind, answer in enumerate(data)]

	plt.plot([f[0] for f in data_as_seconds], [f[1] for f in data_as_seconds])
	plt.show()
